;;; -*- lexical-binding: t -*-
((nil
  . ((eval . (let ((sdks-directory (concat (car (dir-locals-find-file default-directory)) ".yarn/sdks/")))
               (remhash 'prettier format-all--executable-table)
               (puthash 'prettier (concat sdks-directory "prettier/bin/prettier.cjs") format-all--executable-table)))))
 (typescript-mode
  . ((eval . (let ((sdks-directory (concat (car (dir-locals-find-file default-directory)) ".yarn/sdks/")))
               (set (make-local-variable 'flycheck-javascript-eslint-executable)
                    (concat sdks-directory "eslint/bin/eslint.js"))
               (setq lsp-clients-typescript-server-args `("--tsserver-path" ,
                                                          (concat sdks-directory
                                                                  "typescript/bin/tsserver")
                                                          "--stdio")))))))
