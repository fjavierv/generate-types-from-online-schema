#!/usr/bin/env node

import fs from "node:fs";
import path from "node:path";
import { compile } from "json-schema-to-typescript";

const CONFIG_FILE_NAME = "generate-types-from-json-schema.json";

async function fetchJsonFile(url) {
  process.stdout.write(`Fetching ${url}: `);
  const res = await fetch(url, { method: "GET" });
  if (!res.ok) {
    throw new Error(`Error fetching: ${url}`);
  } else {
    const json = await res.json();
    process.stdout.write("OK\n");
    return json;
  }
}

export async function generateTypesForSchema(schema, target, base) {
  // Replace http files for local files using base
  const customResolver = {
    order: 1,
    canRead: true,
    read({ url }, callback) {
      const filename = url.split("/").pop();
      try {
        const schema = fs.readFileSync(`${base}/${filename}`);
        callback(null, schema);
      } catch (err) {
        callback(err);
      }
    },
  };

  try {
    const dirname = path.dirname(target);

    // Create outputDir if not exist
    if (!fs.existsSync(dirname)) {
      fs.mkdirSync(dirname, { recursive: true });
    }

    // Compiling
    process.stdout.write(`Compiling ${target} from ${process.cwd()}: `);

    const result = await compile(schema, "Schema Name", {
      $refOptions: base ? { resolve: { customResolver } } : {},
    });

    fs.writeFileSync(target, result);
    process.stdout.write("OK\n");
  } catch (err) {
    console.error(err);
  }
}

export async function generateTypesForLocalSchema(path, target, base) {
  try {
    const schema = fs.readFileSync(path);
    await generateTypesForSchema(JSON.parse(schema), target, base);
  } catch (err) {
    console.error(err);
  }
}

export async function generateTypesForRemoteSchema(url, target, base) {
  try {
    const schema = await fetchJsonFile(url);
    await generateTypesForSchema(schema, target, base);
  } catch (err) {
    console.error(err);
  }
}

const { default: CONFIG } = await import(
  path.join(process.cwd(), CONFIG_FILE_NAME),
  {
    assert: { type: "json" },
  }
);

for (const { url, target, base } of CONFIG) {
  if (url.startsWith("https://")) {
    await generateTypesForRemoteSchema(url, target, base);
  } else if (url.startsWith("file:///")) {
    const path = url.substring(8);
    await generateTypesForLocalSchema(path, target, base);
  } else {
    process.stderr.write(`Unkown prefix for ${url}, ignoring\n`);
  }
}
